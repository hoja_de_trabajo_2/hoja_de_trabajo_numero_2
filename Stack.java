//*
* @author Nikolas Dimitrio Badani Gasdaglis 20092 
* @fecha 5/2/2021
* @seccion 20 Algoritmos y Estructuras de Datos 
*/ 

public interface Stack<E>
{

    public void push(E item);
    // pre :
    // post : el articulo se agrega al Stack 
    // Aparecera a continuacion si no hay ningun empuje de intermedio

    public E pop();
    // pre : el Stack no esta vacio 
    // post : el articulo que haya sido empujado recientemente es removido y regresado

    public E peek();
    // pre : el Stack no esta vacio 
    // post : el valor máximo (el que continua para aparecer) es regresado

    public boolean empty();
    // post : regresa verdadero, si y solo si, el Stack se encuentra vacio 

    public int size();
    // post : regresar el número de elementos que se encuentren en el Stack 
}

