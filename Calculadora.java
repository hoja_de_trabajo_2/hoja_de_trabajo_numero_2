/**
* @author Nikolas Dimitrio Badani Gasdaglis 20092 
* @fecha 5/2/2021
* @seccion 20 Algoritmos y Estructuras de Datos 
*/

public interface Calculadora {

	public Double Resolver(String algo);
}
