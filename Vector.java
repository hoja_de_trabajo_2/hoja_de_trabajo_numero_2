/**
* @author Nikolas Dimitrio Badani Gasdaglis 20092 
* @fecha 5/2/2021
* @seccion 20 Algoritmos y Estructuras de Datos 
*/

private static void Controlador(String STRoperacion){
	// Instancia de la clase VECTOR, para que de este modo se tome como parametro debido la espeicifacion 
	// de que se trabajara unicamente con enteros.
	VECTOR<Integer> stack = new VECTOR<>();
	// El Forloop se encagara de recorrer cada uno de los pos(n) del STRoperacion[n] con su respectivo metodo length -> int : 
	for(int pos = 0; pos < STRoperacion.length(); pos++){
		// Se utiliza el try para poder castear y empujar cada uno de los integers hacia el stack. Si el proceso tira algun tipo de error
		// es porque se encuentra trabajando con un caracter. En dado caso, este mismo es procesado en el catch. 
		try{
			stack.push( Integer.parseInt(Character.toString(STRoperacion.charAT(pos)))) ;
		} catch (Exception e) {
			// Para este punto en particular, se procesa un caracter cuando por lo menos deben de haber dos enteros almacenados 
			// Por lo cual, se eliminara instantaneamente del Stack y se almacenara con el resto de los ints temporales.
			int number1 = stack.pop();

			int number2 = stack.pop();
			// El caracter en cuestion es analizado en el Switch, reconociendo al instante que clase de signo de operacion se esta empleando.
			// Luego, se realiza la operacion correspondiente entre los dos caracteres (sumar, restar, multiplicar y dividir) 
			// para que finalmente el resultado sea empujado hacia el Stack. 
			switch(STRoperacion.charAT(pos)){
				case '+':
					stack.push(number1 + number2);
				break;
				case '-':
					stack.push(number1 - number2);
				break;
				case '/' :
					stack.push(number1 / number2);
				break;
				case '*'
					stack.push(number1 * number2);
				break;
			}
		}
	}
}


